-- | Type checking.
module Check where

import Data.List

import Core
import Util
import Eval

-- | Type checking contexts.
type Ctx = [Ty]

-- | Compute the type of a term.  Errors if the term is not well-typed.
typeOf :: Term -> Ty
typeOf = typeOf' []

-- | Compute the type of a term in a context.  Errors if the term is not
-- well-typed.
{-# ANN typeOf' "HLint: ignore Redundant multi-way if" #-}
typeOf' :: Ctx -> Term -> Ty
typeOf' ctx TmTrue = TyBool
typeOf' ctx TmFalse = TyBool
typeOf' ctx TmZero = TyNat
typeOf' ctx (TmSucc t1)
  | ty1 <- typeOf' ctx t1
  , tyEquiv ctx ty1 TyNat
  = TyNat
  | otherwise
  = error "succ: argument not a Nat"
typeOf' ctx (TmPred t1)
  | ty1 <- typeOf' ctx t1
  , tyEquiv ctx ty1 TyNat
  = TyNat
  | otherwise
  = error "pred: argument not a Nat"
typeOf' ctx (TmIsZero t1)
  | ty1 <- typeOf' ctx t1
  , tyEquiv ctx ty1 TyNat
  = TyBool
  | otherwise
  = error "iszero: argument not a Nat"
typeOf' ctx TmUnit = TyUnit
typeOf' ctx (TmString _) = TyString
typeOf' ctx (TmFloat _) = TyFloat
typeOf' ctx (TmTimesFloat t1 t2)
  | ty1 <- typeOf' ctx t1
  , ty2 <- typeOf' ctx t2
  , tyEquiv ctx ty1 TyFloat
  = if | tyEquiv ctx ty2 TyFloat -> TyFloat
       | otherwise -> error "timesfloat: second argument not a Float"
  | otherwise = error "timesfloat: first argument not a Float"
typeOf' ctx (TmIf t1 t2 t3)
  | ty1 <- typeOf' ctx t1
  , tyEquiv ctx ty1 TyBool
  = if | ty2 <- typeOf' ctx t2
       , ty3 <- typeOf' ctx t3
       , tyEquiv ctx ty2 ty3 -> ty2
       | otherwise -> error "if: arm types not equal"
  | otherwise = error "if: guard not a Bool"
typeOf' ctx (TmApp t1 t2)
  | ty1 <- typeOf' ctx t1
  , TyArr ty11 ty12 <- simplifyTy ctx ty1
  = if | ty2 <- typeOf' ctx t2
       , tyEquiv ctx ty2 ty11 -> ty12
       | otherwise -> error "app: argument type mismatch"
  | otherwise = error "app: not a function"
typeOf' ctx (TmFix t1)
  | ty1 <- typeOf' ctx t1
  , TyArr ty11 ty12 <- simplifyTy ctx ty1
  = if | tyEquiv ctx ty11 ty12 -> ty11
       | otherwise -> error "fix: source and target types not equal"
  | otherwise = error "fix: not a function"
typeOf' ctx (TmAscribe t1 ty)
  | tyEquiv ctx ty (typeOf' ctx t1)
  = ty
  | otherwise = error "ascribe: wrong type"
typeOf' ctx (TmRecord fields)
  | isUnique (map fst fields)
  = TyRecord [(name, typeOf' ctx ti) | (name, ti) <- fields]
  | otherwise
  = error "record: labels not unique"
typeOf' ctx (TmProj t1 name)
  | ty1 <- typeOf' ctx t1
  , TyRecord fieldTys <- simplifyTy ctx ty1
  = if | Just t1k <- lookup name fieldTys -> t1k
       | otherwise -> error "proj: missing label"
  | otherwise = error "proj: not a record"
typeOf' ctx (TmTag name t1 ty)
  | ty1 <- typeOf' ctx t1
  , TyVariant fieldTys <- simplifyTy ctx ty
  = if | isUnique (map fst fieldTys) ->
         if | Just ty1' <- lookup name fieldTys ->
              if | tyEquiv ctx ty1 ty1' -> ty
                 | otherwise -> error "tag: wrong field type"
            | otherwise -> error "tag: missing label"
       | otherwise -> error "tag: labels not unique"
  | otherwise = error "tag: not a variant"
typeOf' ctx (TmCase t1 cases)
  | ty1 <- typeOf' ctx t1
  , TyVariant variants <- simplifyTy ctx ty1
  = if | sortedCases <- sortOn (\(tag, _, _) -> tag) cases
       , sortedVariants <- sortOn fst variants
       , [tag | (tag, _, _) <- cases] == [tag | (tag, _) <- variants] -> if
           | caseTypes <- [typeOf' (tyi : ctx) ti
                          | ((_, _, ti), (_, tyi)) <- zip sortedCases sortedVariants]
           , [ty] <- nub caseTypes -> ty
           | otherwise -> error "case: arm types not all equal"
       | otherwise -> error "case: labels do not match"
  | otherwise = error "case: not a variant"
typeOf' ctx (TmAbs _ ty1 t1)
  | ty2 <- typeOf' (ty1 : ctx) t1
  = TyArr ty1 ty2
typeOf' ctx (TmLet _ t1 t2)
  | ty1 <- typeOf' ctx t1
  , ty2 <- typeOf' (ty1 : ctx) t2
  = ty2
typeOf' ctx (TmVar x)
  | Just ty <- ctx !? x
  = ty
  | otherwise
  = error "var: not bound"

simplifyTy :: Ctx -> Ty -> Ty
simplifyTy ctx ty = iterateWhileJust go ty where
  go ty@(TyRec _ ty1) = Just (instantiateTy ty ty1)
  go (TyVar x) = Just (ctx !! x)
  go _ = Nothing

tyEquiv :: Ctx -> Ty -> Ty -> Bool
tyEquiv ctx = go [] where
  go seen t1 t2 | (t1, t2) `elem` seen = True
  go seen TyBool TyBool = True
  go seen TyNat TyNat = True
  go seen TyUnit TyUnit = True
  go seen TyString TyString = True
  go seen TyFloat TyFloat = True
  go seen (TyId s1) (TyId s2) = s1 == s2
  go seen (TyArr ty11 ty12) (TyArr ty21 ty22) =
    go seen ty11 ty21 && go seen ty12 ty22
  go seen (TyRecord fs1) (TyRecord fs2) =
    length fs1 == length fs2 &&
    all (\((n1, ty1i), (n2, ty2i)) -> n1 == n2 && go seen ty1i ty2i)
    (zip (sortOn fst fs1) (sortOn fst fs2))
  go seen (TyVariant vs1) (TyVariant vs2) =
    length vs1 == length vs1 &&
    all (\((t1, ty1i), (t2, ty2i)) -> t1 == t2 && go seen ty1i ty2i)
    (zip (sortOn fst vs1) (sortOn fst vs2))
  go seen ty1@(TyRec _ ty11) ty2 = go ((ty1, ty2) : seen) (instantiateTy ty1 ty11) ty2
  go seen ty1 ty2@(TyRec _ ty21) = go ((ty1, ty2) : seen) ty1 (instantiateTy ty2 ty21)
  go seen (TyVar x1) t2 | x1 < length ctx = go seen (ctx !! x1) t2
  go seen t1 (TyVar x2) | x2 < length ctx = go seen t1 (ctx !! x2)
  go seen (TyVar x1) (TyVar x2) = x1 == x2
  go seen _ _ = False
