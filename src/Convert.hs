-- | Convert named types and terms to unnamed types and terms.
module Convert where

import Data.Maybe
import Data.List

import Core

type Ctx = [String]

-- | Convert a named type to an unnamed (de Bruijn-indexed) type.
named2UnnamedTy :: NamedTy -> Ty
named2UnnamedTy = named2UnnamedTy' []

-- | Convert a named type to an unnamed (de Bruijn-indexed) type in a given
-- context.
named2UnnamedTy' :: Ctx -> NamedTy -> Ty
named2UnnamedTy' ctx TyBool = TyBool
named2UnnamedTy' ctx TyNat = TyNat
named2UnnamedTy' ctx TyUnit = TyUnit
named2UnnamedTy' ctx TyString = TyString
named2UnnamedTy' ctx TyFloat = TyFloat
named2UnnamedTy' ctx (TyArr ty1 ty2) =
  TyArr (named2UnnamedTy' ctx ty1) (named2UnnamedTy' ctx ty2)
named2UnnamedTy' ctx (TyRecord fields) =
  TyRecord [(name, named2UnnamedTy' ctx tyk) | (name, tyk) <- fields]
named2UnnamedTy' ctx (TyVariant variants) =
  TyVariant [(name, named2UnnamedTy' ctx tyk) | (name, tyk) <- variants]
named2UnnamedTy' ctx (TyRec x ty1) = TyRec x (named2UnnamedTy' (x : ctx) ty1)
named2UnnamedTy' ctx (TyVar x)
  | Just x' <- elemIndex x ctx = TyVar x'
  | otherwise = TyId x

-- | Convert a named term to an unnamed (de Bruijn-indexed) term.
named2UnnamedTerm :: NamedTerm -> Term
named2UnnamedTerm = named2UnnamedTerm' [] []

-- | Convert a named term to an unnamed (de Bruijn-indexed) term in type/term
-- contexts.
named2UnnamedTerm' :: Ctx -> Ctx -> NamedTerm -> Term
named2UnnamedTerm' tyCtx ctx TmTrue = TmTrue
named2UnnamedTerm' tyCtx ctx TmFalse = TmFalse
named2UnnamedTerm' tyCtx ctx TmZero = TmZero
named2UnnamedTerm' tyCtx ctx TmUnit = TmUnit
named2UnnamedTerm' tyCtx ctx (TmString s) = TmString s
named2UnnamedTerm' tyCtx ctx (TmFloat n) = TmFloat n
named2UnnamedTerm' tyCtx ctx (TmIf t1 t2 t3) = TmIf
  (named2UnnamedTerm' tyCtx ctx t1)
  (named2UnnamedTerm' tyCtx ctx t2)
  (named2UnnamedTerm' tyCtx ctx t3)
named2UnnamedTerm' tyCtx ctx (TmSucc t1) = TmSucc (named2UnnamedTerm' tyCtx ctx t1)
named2UnnamedTerm' tyCtx ctx (TmPred t1) = TmPred (named2UnnamedTerm' tyCtx ctx t1)
named2UnnamedTerm' tyCtx ctx (TmIsZero t1) = TmIsZero (named2UnnamedTerm' tyCtx ctx t1)
named2UnnamedTerm' tyCtx ctx (TmTimesFloat t1 t2) =
  TmTimesFloat (named2UnnamedTerm' tyCtx ctx t1) (named2UnnamedTerm' tyCtx ctx t2)
named2UnnamedTerm' tyCtx ctx (TmApp t1 t2) =
  TmApp (named2UnnamedTerm' tyCtx ctx t1) (named2UnnamedTerm' tyCtx ctx t2)
named2UnnamedTerm' tyCtx ctx (TmAscribe t1 ty1) =
  TmAscribe (named2UnnamedTerm' tyCtx ctx t1) (named2UnnamedTy' tyCtx ty1)
named2UnnamedTerm' tyCtx ctx (TmRecord fields) =
  TmRecord [(name, named2UnnamedTerm' tyCtx ctx value) | (name, value) <- fields]
named2UnnamedTerm' tyCtx ctx (TmProj t1 field) = TmProj
  (named2UnnamedTerm' tyCtx ctx t1)
  field
named2UnnamedTerm' tyCtx ctx (TmTag variant value ty1) =
  TmTag variant (named2UnnamedTerm' tyCtx ctx value) (named2UnnamedTy' tyCtx ty1)
named2UnnamedTerm' tyCtx ctx (TmCase t1 cases) = TmCase
  (named2UnnamedTerm' tyCtx ctx t1)
  [ (label, x, named2UnnamedTerm' tyCtx (x : ctx) tk)
  | (label, x, tk) <- cases
  ]
named2UnnamedTerm' tyCtx ctx (TmFix t1) = TmFix (named2UnnamedTerm' tyCtx ctx t1)
named2UnnamedTerm' tyCtx ctx (TmAbs x ty1 t1) =
  TmAbs x (named2UnnamedTy' tyCtx ty1) (named2UnnamedTerm' tyCtx (x : ctx) t1)
named2UnnamedTerm' tyCtx ctx (TmLet x t1 t2) =
  TmLet x (named2UnnamedTerm' tyCtx ctx t1) (named2UnnamedTerm' tyCtx (x : ctx) t2)
named2UnnamedTerm' tyCtx ctx (TmVar x)
  | Just x' <- elemIndex x ctx = TmVar x'
  | otherwise = TmVar (length ctx)
