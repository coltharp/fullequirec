module Core where

data Ty' tv
  = TyBool
  | TyNat
  | TyUnit
  | TyString
  | TyFloat
  | TyArr (Ty' tv) (Ty' tv)
  | TyRecord [(String, Ty' tv)]
  | TyVariant [(String, Ty' tv)]
  | TyRec String (Ty' tv)
  | TyVar tv
  | TyId String
  deriving (Eq, Show)

type Ty = Ty' Int
type NamedTy = Ty' String

data Term' tv v
  = TmTrue
  | TmFalse
  | TmIf (Term' tv v) (Term' tv v) (Term' tv v)
  | TmZero
  | TmSucc (Term' tv v)
  | TmPred (Term' tv v)
  | TmIsZero (Term' tv v)
  | TmUnit
  | TmString String
  | TmFloat Double
  | TmRecord [(String, Term' tv v)]
  | TmTag String (Term' tv v) (Ty' tv)
  | TmAscribe (Term' tv v) (Ty' tv)
  | TmVar v
  | TmTimesFloat (Term' tv v) (Term' tv v)
  | TmLet String (Term' tv v) (Term' tv v)
  | TmAbs String (Ty' tv) (Term' tv v)
  | TmApp (Term' tv v) (Term' tv v)
  | TmFix (Term' tv v)
  | TmProj (Term' tv v) String
  | TmCase (Term' tv v) [(String, String, Term' tv v)]
  deriving (Eq, Show)

type Term = Term' Int Int
type NamedTerm = Term' String String
