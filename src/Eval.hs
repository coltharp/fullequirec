module Eval where

import Core

type Renaming = Int -> Int

liftRenaming :: Renaming -> Renaming
liftRenaming r = \case
  0 -> 0
  x -> r (x - 1) + 1

renameTy :: Renaming -> Ty -> Ty
renameTy r (TyVar x) = TyVar (r x)
renameTy r (TyRec x ty1) = TyRec x (renameTy (liftRenaming r) ty1)
renameTy r (TyArr ty1 ty2) = TyArr (renameTy r ty1) (renameTy r ty2)
renameTy r (TyRecord fields) = TyRecord [(name, renameTy r tyi) | (name, tyi) <- fields]
renameTy r (TyVariant variants) = TyVariant [(tag, renameTy r tyi) | (tag, tyi) <- variants]
renameTy r ty = ty

type TySub = Int -> Ty

infixr 5 <|
(<|) :: Ty -> TySub -> TySub
ty <| s = \case
  0 -> ty
  x -> s (x - 1)

liftTySub :: TySub -> TySub
liftTySub s = TyVar 0 <| renameTy (+ 1) . s

tySub :: TySub -> Ty -> Ty
tySub s (TyVar x) = s x
tySub s (TyRec x ty1) = TyRec x (tySub (liftTySub s) ty1)
tySub s (TyArr t1 t2) = TyArr (tySub s t1) (tySub s t2)
tySub s (TyRecord fields) = TyRecord [(name, tySub s tyi) | (name, tyi) <- fields]
tySub s (TyVariant variants) = TyVariant [(tag, tySub s tyi) | (tag, tyi) <- variants]
tySub s ty = ty

instantiateTy :: Ty -> Ty -> Ty
instantiateTy t2 = tySub (t2 <| TyVar)

-- typeSubst :: Ty -> Ty -> Ty

-- bv :: Term -> Bool
-- bv TmTrue = True
-- bv TmFalse = True
-- bv _ = False

-- nv :: Term -> Bool
-- nv TmZero = True
-- nv (TmSucc t1) = nv t1
-- nv _ = False

-- uv :: Term -> Bool
-- uv TmUnit = True
-- uv _ = False

-- sv :: Term -> Bool
-- sv (TmString _) = True
-- sv _ = False

-- fv :: Term -> Bool
-- fv (TmFloat _) = True
-- fv _ = False

-- rv :: Term -> Bool
-- rv (TmRecord fields) = all (isValue . snd) fields
-- rv _ = False

-- vv :: Term -> Bool
-- vv (TmTag _ t1 _) = isValue t1

-- isValue :: Term -> Bool
-- isValue v = any ($ v) [bv, nv, uv, sv, fv, rv, vv]

-- type Ctx = [Term]

-- eval1 :: Term -> Term
-- eval1 = eval1' []

-- eval1' :: Ctx -> Term -> Term
-- -- eval1' ctx (TmIf TmTrue t2 t3)
-- --   = t2
-- -- eval1' ctx (TmIf TmFalse t2 t3)
-- --   = t3
-- -- eval1' ctx (TmIf t1 t2 t3)
-- --   | t1' <- eval1' ctx t1
-- --   = TmIf t1' t2 t3
-- -- eval1' ctx (TmSucc t1)
-- --   | t1' <- eval1' ctx  t1
-- --   = TmSucc t1'
-- -- eval1' ctx (TmPred TmZero)
-- --   = TmZero
-- -- eval1' ctx (TmPred (TmSucc nv1))
-- --   | nv nv1
-- --   = nv1
-- -- eval1' ctx (TmPred t1)
-- --   | t1' <- eval1' ctx t1
-- --   = TmPred t1'
-- -- eval1' ctx (TmIsZero TmZero)
-- --   = TmTrue
-- -- eval1' ctx (TmIsZero (TmSucc nv1))
-- --   | nv nv1
-- --   = TmFalse
-- -- eval1' ctx (TmIsZero t1)
-- --   | t1' <- eval1' ctx t1
-- --   = TmIsZero t1'
-- -- eval1' ctx (TmAscribe v1 ty1)
-- --   | isValue v1 = v1
-- -- eval1' ctx (TmAscribe t1 ty1)
-- --   | t1' <- eval1' ctx t1 = TmAscribe t1' ty1
