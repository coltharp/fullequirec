module PP (PP, pp) where

import Util
import Core

import Data.List

parens :: ShowS -> ShowS
parens p = ppS "(" <> p <> ppS ")"

class PP a where
  ppS :: a -> ShowS

pp :: PP a => a -> String
pp = flip ppS ""

instance PP String where
  ppS = showString

instance PP Int where
  ppS = shows

instance PP Double where
  ppS = shows

type Ctx = [String]

instance PP Ty where
  ppS = go [] False where
    go ctx _ TyBool = ppS "Bool"
    go ctx _ TyNat = ppS "Nat"
    go ctx _ TyUnit = ppS "Unit"
    go ctx _ TyString = ppS "String"
    go ctx _ TyFloat = ppS "Float"
    go ctx _ (TyRecord ls) = ppS "{" <> ppLabelsS ctx ls <> ppS "}"
    go ctx _ (TyVariant ls) = ppS "<" <> ppLabelsS ctx ls <> ppS ">"
    go ctx _ (TyVar x)
      | Just x' <- ctx !? x = ppS x'
      | otherwise = ppS "#Free"
    go ctx _ (TyId x) = ppS x
    go ctx b ty = (if b then parens else id) (go' ctx ty)

    go' ctx (TyArr t1 t2) = go ctx True t1 <> showString " -> " <> go ctx False t2
    go' ctx (TyRec x t1) = ppS "Rec " <> ppS x <> ppS ". " <> go (x : ctx) False t1

    ppLabelsS ctx ls =
      mconcat (intersperse (showString ", ")
               [(if show i == l then id else ((ppS l <> ppS ": ") <>)) (go ctx False ty)
               | (i, (l, ty)) <- zip [1..] ls])

-- | Syntactic term contexts.
data TermSCtx
  -- | Top-level.  No parentheses required.
  = Top
  -- | Left branch of an application.  Parentheses required for complex
  -- expressions, except for other applications.
  | AppL
  -- | Simple expression expected.  Parentheses required for complex
  -- expressions.
  | Simple

isTop :: TermSCtx -> Bool
isTop Top = True
isTop _ = False

isSimple :: TermSCtx -> Bool
isSimple Simple = True
isSimple _ = False

instance PP Term where
  ppS = go [] Top where
    go :: Ctx -> TermSCtx -> Term -> ShowS
    go _ _ TmTrue = ppS "true"
    go _ _ TmFalse = ppS "false"
    go _ _ TmZero = ppS "0"
    go _ _ TmUnit = ppS "unit"
    go _ _ (TmString x) = shows x
    go _ _ (TmFloat x) = ppS x
    go ctx _ (TmRecord fs) =
      ppS "{"
      <> mconcat (intersperse (ppS ", ")
                  [(if show i == l then id else ((ppS l <> ppS " = ") <>)) (go ctx Top t)
                  | (i, (l, t)) <- zip [1..] fs]) <> ppS "}"
    go ctx _ (TmTag x t1 ty) = ppS "<" <> ppS x <> ppS " = " <> go ctx Top t1 <> ppS "> as "
      <> ppS ty
    go ctx _ (TmAscribe t1 ty) = go ctx Simple t1 <> ppS " as " <> ppS ty
    go ctx _ (TmVar x)
      | Just x' <- ctx !? x = ppS x'
      | otherwise = ppS "#free"
    go ctx _ (TmProj t1 l) = go ctx Simple t1 <> ppS "." <> ppS l
    go ctx b t@(TmSucc _) = ppNum ctx b t
    go ctx sctx (TmApp t1 t2) =
      (if isSimple sctx then parens else id) (go ctx AppL t1 <> ppS " " <> go ctx Simple t2)
    go ctx sctx t = (if isTop sctx then id else parens) (go' ctx t)

    go' ctx (TmIf t1 t2 t3) =
      ppS "if " <> go ctx Top t1 <> ppS " then " <> go ctx Top t2 <> ppS " else "
      <> go ctx Top t3
    go' ctx (TmPred t1) = ppS "pred " <> go ctx Simple t1
    go' ctx (TmIsZero t1) = ppS "iszero " <> go ctx Simple t1
    go' ctx (TmTimesFloat t1 t2) =
      ppS "timesfloat " <> go ctx Simple t1 <> ppS " " <> go ctx Simple t2
    go' ctx (TmLet x t1 t2) =
      ppS "let " <> ppS x <> ppS " = " <> go ctx Top t1 <> ppS " in " <> go (x : ctx) Top t2
    go' ctx (TmAbs x ty t1) =
      ppS "lambda " <> ppS x <> ppS ": " <> ppS ty <> ppS ". " <> go (x : ctx) Top t1
    go' ctx (TmApp t1 t2) = go ctx AppL t1 <> ppS " " <> go ctx Simple t2
    go' ctx (TmFix t1) = ppS "fix " <> ppS t1
    go' ctx (TmCase t1 cs) = ppS "case " <> go ctx Top t1 <> ppS " of " <>
      mconcat (intersperse (ppS " | ")
               [ppS "<" <> ppS x <> ppS " = " <> ppS p <> ppS "> ==> " <> go (x : ctx) Top tn
               | (x, p, tn) <- cs])

    ppNum ctx sctx t = case ppNum' ctx sctx t of
      Left s -> s
      Right n -> ppS n

    ppNum' :: Ctx -> TermSCtx -> Term -> Either ShowS Int
    ppNum' ctx _ TmZero = Right 0
    ppNum' ctx sctx (TmSucc t1) = case ppNum' ctx Simple t1 of
      Left s -> Left ((if isTop sctx then id else parens) (ppS "succ " <> s))
      Right n -> Right (n + 1)
    ppNum' ctx b t = Left (go ctx b t)

    toNum TmZero = Just 0
    toNum (TmSucc t) = (1 +) <$> toNum t
    toNum _ = Nothing
