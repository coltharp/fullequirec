module Parse where

import Prelude hiding (lex)
import Data.Char
import Data.Maybe
import Control.Monad
import Control.Monad.Combinators.Expr
import Text.Megaparsec
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer qualified as Lexer

import Core
import Util
import Convert

parseTerm :: String -> Term
parseTerm s = fromJust (parseMaybe @() term s)

parseTerm' :: Ctx -> Ctx -> String -> Term
parseTerm' tyCtx ctx = fromJust . parseMaybe @() (term' tyCtx ctx)

term :: Ord e => Parsec e String Term
term = named2UnnamedTerm <$> namedTerm

term' :: Ord e => Ctx -> Ctx -> Parsec e String Term
term' tyCtx ctx = named2UnnamedTerm' tyCtx ctx <$> namedTerm

namedTerm :: Ord e => Parsec e String NamedTerm
namedTerm = term where

  term = tmIf
    <|> tmSucc
    <|> tmPred
    <|> tmIsZero
    <|> tmTimesFloat
    <|> tmFix
    <|> tmAbs
    <|> tmLet
    <|> tmCase
    <|> makeExprParser simpleTerm [[InfixL (pure TmApp)]]

  simpleTerm = do
    t <- verySimpleTerm
    (TmProj t <$> (sym "." *> proj)) <|> pure t

  verySimpleTerm = do
    t <- extremelySimpleTerm
    (TmAscribe t <$> (key "as" *> namedTy)) <|> pure t

  extremelySimpleTerm = tmTrue
    <|> tmFalse
    <|> tmFloat
    <|> tmNumeral
    <|> tmUnit
    <|> tmString
    <|> tmRecord
    <|> tmTag
    <|> tmVar
    <|> parens term

  tmIf = TmIf <$ key "if" <*> term <* key "then" <*> term <* key "else" <*> term
  tmSucc = TmSucc <$ key "succ" <*> simpleTerm
  tmPred = TmPred <$ key "pred" <*> simpleTerm
  tmIsZero = TmIsZero <$ key "iszero" <*> simpleTerm
  tmTimesFloat = TmTimesFloat <$ key "timesfloat" <*> simpleTerm <*> simpleTerm
  tmFix = TmFix <$ key "fix" <*> simpleTerm
  tmAbs = TmAbs <$ key "lambda" <*> ident <* sym ":" <*> namedTy <* sym "." <*> term
  tmLet = TmLet <$ key "let" <*> ident <* sym "=" <*> term <* key "in" <*> term
  tmCase = TmCase <$ key "case" <*> term <* key "of" <*>
    ((,,) <$ sym "<" <*> ident <* sym "=" <*> ident <* sym ">" <* sym "==>" <*> term)
    `sepBy` sym "|"

  tmTrue = TmTrue <$ key "true"
  tmFalse = TmFalse <$ key "false"
  tmNumeral = elimNat TmSucc TmZero <$> numeral
  tmUnit = TmUnit <$ key "unit"
  tmString = TmString <$ char '"' <*> Lexer.charLiteral `manyTill` tokChar '"'
  tmFloat = TmFloat <$> (try . lex . delimited $ Lexer.float)
  tmRecord = do
    fields <- braces (tmField `sepBy` sym ",")
    pure $ TmRecord [case field of
                       Left (name, term) -> (name, term)
                       Right term -> (show i, term)
                    | (i, field) <- zip [1..] fields]
  tmField = try (curry Left <$> ident <* sym "=" <*> term) <|> (Right <$> term)
  tmTag = TmTag <$ sym "<" <*> ident <* sym "=" <*> term <* sym ">" <* key "as" <*> namedTy
  tmVar = try (TmVar <$> ident)

  numeral = lex . delimited $ Lexer.decimal

  proj = ident <|> (show <$> numeral)

  tokChar c = tok (char c)
  tok p = p <* space

parseTy :: String -> Ty
parseTy = fromJust . parseMaybe @() ty

parseTy' :: Ctx -> String -> Ty
parseTy' ctx = fromJust . parseMaybe @() (ty' ctx)

ty :: Ord e => Parsec e String Ty
ty = named2UnnamedTy <$> namedTy

ty' :: Ord e => Ctx -> Parsec e String Ty
ty' ctx = named2UnnamedTy' ctx <$> namedTy

namedTy :: Ord e => Parsec e String NamedTy
namedTy = ty where

  ty = makeExprParser ty' [[InfixR tyArr]]

  tyArr = TyArr <$ sym "->"
  ty' = tyBool
    <|> tyNat
    <|> tyUnit
    <|> tyString
    <|> tyFloat
    <|> tyRec
    <|> tyRecord
    <|> tyVariant
    <|> tyVar
    <|> parens ty

  tyBool = TyBool <$ key "Bool"
  tyNat = TyNat <$ key "Nat"
  tyUnit = TyUnit <$ key "Unit"
  tyString = TyString <$ key "String"
  tyFloat = TyFloat <$ key "Float"
  tyRec = TyRec <$ key "Rec" <*> ident <* sym "." <*> ty
  tyRecord = do
    fields <- braces (tyField `sepBy` sym ",")
    pure $ TyRecord [case field of
                       Left (name, ty) -> (name, ty)
                       Right ty -> (show i, ty)
                    | (i, field) <- zip [1..] fields]
  tyVariant = TyVariant <$> between (sym "<") (sym ">")
    (((,) <$> ident <* sym ":" <*> ty) `sepBy` sym ",")
  tyField = try (curry Left <$> ident <* sym ":" <*> ty) <|> (Right <$> ty)
  tyVar = TyVar <$> ident

ident :: Ord e => Parsec e String String
ident = lex . delimited $ do
  hd <- satisfy (\c -> isAlpha c || c == '_')
  tl <- takeWhileP Nothing (\c -> isAlphaNum c || c == '_')
  let ret = hd : tl
  guard (ret `notElem` keys)
  return ret

parens :: Ord e => Parsec e String a -> Parsec e String a
parens = between (sym "(") (sym ")")

braces :: Ord e => Parsec e String a -> Parsec e String a
braces = between (sym "{") (sym "}")

sym :: Ord e => String -> Parsec e String String
sym = Lexer.symbol space

key :: Ord e => String -> Parsec e String String
key = try . lex . delimited . string

delimited p =
  p <* (eof <|> void (lookAhead (satisfy (\c -> c `elem` "(),-.:;<=>{|}" || isSpace c))))

lex :: Ord e => Parsec e String a -> Parsec e String a
lex = Lexer.lexeme space

keys = [ "Bool"
       , "Float"
       , "Nat"
       , "Rec"
       , "String"
       , "Unit"
       , "as"
       , "case"
       , "else"
       , "false"
       , "fix"
       , "if"
       , "in"
       , "iszero"
       , "lambda"
       , "let"
       , "of"
       , "succ"
       , "then"
       , "timesfloat"
       , "true"
       , "unit"
       , "zero"
       ]
