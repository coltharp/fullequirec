module Util where

import Data.List

elimNat :: (Eq b, Num b) => (a -> a) -> a -> b -> a
elimNat f e 0 = e
elimNat f e n = f (elimNat f e (n - 1))

iterateWhileJust :: (a -> Maybe a) -> a -> a
iterateWhileJust f x
  | Just x' <- f x = iterateWhileJust f x'
  | otherwise = x

-- This operator is in base-4.19/GHC 9.8, but there is currently (May 2024) not
-- a Stackage snapshot for that.
infixl 9 !?
(!?) :: [a] -> Int -> Maybe a
[] !? _ = Nothing
(x : xs) !? 0 = Just x
(x : xs) !? n = xs !? (n - 1)

isUnique :: Ord a => [a] -> Bool
isUnique xs =
  let sorted = sort xs
  in map head (group sorted) == sorted
